#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 09:59:26 2022

@author: swtoskon
"""

import psycopg2
from psycopg2 import Error

vessels_ = {"LNG Carrier" : 1 , "Tanker" : 1 ,"Crude Oil Carrier": 1 , "crude_oil_tanker": 1,"oil_products_tanker":1,"chemical_oil_products_tanker":2, "Chemical Tanker" :2, "chemical_tanker" :2,
             "LPG Carrier" : 2,  "Product carrier":2 ,"Bulk Carrier" : 3, "Dry bulk carrier" : 3 ,"bulk_carrier" : 3 ,"General Dry Cargo" : 4 , "Cargo ship": 4,
            "Reefer" : 5, "Container Ship" : 5 , "Container" : 5 , "container_ship" : 5}


 # Connect to  database
try:
    connection = psycopg2.connect(user='skonstantakos',
                                  password='sotiris_db_pass_2022',
                                  host="62.103.77.71",
                                  port="11001",
                                  database="vb_ais_database",
                                 )
    connection.autocommit = True
    cursor = connection.cursor()
except (Exception, Error) as error:
    print("Error while connecting to PostgreSQL", error)
    
mmsi1='211845000'
sql= "SELECT * FROM spire_particulars_v1 WHERE mmsi = '%s'" % mmsi1
cursor.execute(sql)
row = cursor.fetchone()
GT = row[1]['gross_tonnage']
vessel_type=row[1]['_embedded']['enhanced_data']['_embedded']['vessel_and_trading_type']['vessel_type']
vessel_type_number = vessels_[vessel_type]
DWT = row[1]['_embedded']['enhanced_data']['_embedded']['capacity']['dwt']
IMO = row[1]['imo']
TEU= row[1]['_embedded']['enhanced_data']['_embedded']['capacity']['teu']
MCR = row[1]['_embedded']['enhanced_data']['_embedded']['propulsions']['mco']

if vessel_type_number==1:#tanker
    A = 8.1358
    C = 0.05383
    D = 22.8415
    F = 0.55826
    a = 1218.8
    c = 0.488
    if DWT>200000:
        referenceRate=0.15
    elif DWT<200000 and DWT>=20000:
        referenceRate=0.2                
    elif DWT<20000 and DWT>=4000:
        referenceRate=0.2*(DWT-4000)/16000
    else:
        referenceRate=0
elif vessel_type_number==2:#gas
    A = 7.4462
    C = 0.07604
    D = 21.4704
    F = 0.59522
    a = 1120.00 
    c = 0.456
    if DWT>=15000:
        referenceRate=0.3
    elif DWT<10000 and DWT>=15000:
        referenceRate=0.2
    elif DWT<10000 and DWT>=2000:
        referenceRate=0.2*DWT/10000-0.2
    else:
        referenceRate=0
elif vessel_type_number==3:#bulk
    A = 10.6585
    C = 0.02706
    D = 23.7510
    F = 0.54087
    a = 961.79
    c = 0.477
    if DWT>=200000:
        referenceRate=0.15
    elif DWT<200000 and DWT>=20000:
        referenceRate=0.2
    elif DWT<20000 and DWT>=10000:
        referenceRate=0.2*DWT/10000-0.2
    else:
        referenceRate=0
elif vessel_type_number==4:#cargo
    A = 2.4538
    C = 0.18832
    D = 0.8816
    F = 0.92050
    a = 107.48
    c = 0.216
    if DWT>15000:
        referenceRate=0.3
    elif DWT<15000 and DWT>=3000:
        referenceRate=0.3*(DWT-3000)/12000
    else:
        referenceRate=0
elif vessel_type_number==5:#container
    A = 3.2395
    C = 0.18294
    D = 0.5042
    F = 1.03046
    a = 174.22
    c = 0.201
    if DWT>200000:
        referenceRate=0.5
    elif DWT<200000 and DWT>=120000:
        referenceRate=0.45                
    elif DWT<120000 and DWT>=80000:
        referenceRate=0.35
    elif DWT<80000 and DWT>=40000:
        referenceRate=0.3            
    elif DWT<40000 and DWT>=15000:
        referenceRate=0.2
    elif DWT<15000 and DWT>10000:
        referenceRate=0.2*(DWT-10000)/5000
    else:
        referenceRate=0
        
   
#Default values, do not have a test report in the NOx Technical File     
SFC_ME=190
SFC_AE=215 
#no shalf generator
PTO=0  
#for diesel ships
CF=3.114
PME = 0.75*(MCR-PTO)
if vessel_type_number==5:
    if DWT<=95000:    
        MCR_avg = D*(DWT**(F))
    else:
        MCR_avg = D*(95000**(F))
else:
    MCR_avg = D*(DWT**(F))

if vessel_type_number==5:
    if DWT<=80000:
        vavg = A*(DWT**(C))
    else:
        vavg = A*(80000**(C))
else:
        vavg = A*(DWT**(C))
        
#no shalf motors
if MCR>=10000:
    PAE=0.025*MCR+250
else:
    PAE=0.05*MCR
    
vs_eedi=0 #sea trial service speed
ps_eedi=0
margin = min(1,vavg*0.05)
if vs_eedi == 0:
    vapp = (vavg-margin)*(PME/(0.75*MCR_avg))**(1/3)
else:
    vapp = vs_eedi*(PME/ps_eedi)**(1/3)
    

#Froro-1
#fi=1+0.08*LWT_value/DWT_value ; Ships under the Common Structural Rules (CSR)
#fcubic=1
    
#CALCULATE attainden EEXI
if vessel_type_number==5:
    attainedEEXI=(CF*SFC_ME*PME+CF*SFC_AE*PAE)/(0.7*DWT*vapp)       
else:
    attainedEEXI=(CF*SFC_ME*PME+CF*SFC_AE*PAE)/(DWT*vapp)    
    
#calculate required EEXI
referenceline=a*DWT**(-c)
requiredEEXI = referenceline*(1-referenceRate)

print("Attained EEXI is:  " , attainedEEXI)
print("Required EEXI is: " , requiredEEXI)

attainedEEXI_new=attainedEEXI
PME_new = PME
Vappi = vapp
attainedEEXI_new2=0
while attainedEEXI_new-requiredEEXI>0.001:
    PME_new2 = ((0.9999*1/(attainedEEXI_new/requiredEEXI))**(3/2))*PME_new
    Vappi2 = Vappi*(PME_new2/PME_new)**(1/3)
    if vessel_type_number==5:
        attainedEEXI_new2=(CF*SFC_ME*PME_new2+CF*SFC_AE*PAE)/(0.7*DWT*Vappi2)       
    else:
        attainedEEXI_new2=(CF*SFC_ME*PME_new2+CF*SFC_AE*PAE)/(DWT*Vappi2)       
    PME_new = PME_new2
    Vappi = Vappi2
    attainedEEXI_new = attainedEEXI_new2
    print(attainedEEXI_new-requiredEEXI)
    
print("Old Attained EEXI is:  " ,  attainedEEXI)
print("New Attained EEXI is:  " , attainedEEXI_new)


